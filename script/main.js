"use strict";
const servicesParagraph = document.querySelector(".services-wrapper");
const servicesTitle = document.querySelector(".services-title");
const servicesImg = document.querySelectorAll(".services-wrapper img");
//mouseup тому що якщо при click почати виділяти текст у двох або більше li у всіх елементів видаляється клас
servicesTitle.addEventListener("mouseup", function (e) {
  let target = e.target;
  let arr = [...servicesTitle.children];
  arr.forEach((e) => {
    if (e !== target) {
      e.classList.remove("services-item-active");
    } else {
      e.classList.add("services-item-active");
    }
  });
  let arrParagraph = [...servicesParagraph.children];
  arrParagraph.forEach((e) => {
    if (e.dataset.name !== target.dataset.name) {
      e.classList.remove("services-text-active");
    } else {
      e.classList.add("services-text-active");
    }
  });
  let arrImg = [...servicesImg];
  arrImg.forEach((e) => {
    if (e.dataset.name !== target.dataset.name) {
      e.classList.remove("services-img-active");
    } else {
      e.classList.add("services-img-active");
    }
  });
});
//переключання між вкладками

$(".work-list  li").on("click", function () {
  $(".work-list li").removeClass("work-items-active");
  $(this).addClass("work-items-active");
  $(".work-images li").each((i, elem) => {
    $(elem).data("name") !== $(this).data("name")
      ? $(elem).css("display", "none")
      : $(elem).css("display", "flex");
  });
  if ($(this).attr("data-name") === "all") {
    $(".work-images li").css("display", "flex");
  }
});
//додавання ще 12 картинок
{
  let allClone = $(".work-images li").clone();
  const workBtn = $(".work-section-button");
  let img = $(".work-images li").children("img");
  $(img).get(0).src = "./img/wordpress/wordpress9.jpg";
  $(img).get(1).src = "./img/landing-page/landing-page6.jpg";
  $(img).get(2).src = "./img/graphic-design/graphic-design11.jpg";
  $(img).get(3).src = "./img/wordpress/wordpress10.jpg";
  $(img).get(4).src = "./img/graphic-design/graphic-design10.jpg";
  $(img).get(5).src = "./img/landing-page/landing-page7.jpg";
  $(img).get(6).src = "./img/web-design/web-design7.jpg";
  $(img).get(7).src = "./img/wordpress/wordpress4.jpg";
  $(img).get(8).src = "./img/graphic-design/graphic-design12.jpg";
  $(img).get(9).src = "./img/landing-page/landing-page5.jpg";
  $(img).get(10).src = "./img/web-design/web-design6.jpg";
  $(img).get(11).src = "./img/wordpress/wordpress5.jpg";
  let wordPressClone = [];
  let landingClone = [];
  let graphicClone = [];
  let webClone = [];
  $(allClone).each((i, elem) => {
    if ($(elem).data("name") === "wordpress") {
      wordPressClone.push($(elem));
    } else if ($(elem).data("name") === "landing") {
      landingClone.push($(elem));
    } else if ($(elem).data("name") === "graphic") {
      graphicClone.push($(elem));
    } else if ($(elem).data("name") === "web") {
      webClone.push($(elem));
    }
  });
  $(workBtn).on("click", function () {
    $(".loader").css("display", "flex");
    let data = $(".work-items-active").data("name");
    $(workBtn).css("display", "none");
    setTimeout(() => {
      if (data === "wordpress") {
        $(".work-images").append(wordPressClone);
      } else if (data === "graphic") {
        $(".work-images").append(graphicClone);
      } else if (data === "all") {
        $(".work-images").append(allClone);
      } else if (data === "landing") {
        $(".work-images").append(landingClone);
      } else if (data === "web") {
        $(".work-images").append(webClone);
      }
      $(".loader").css("display", "none");
    }, 2000);
  });
}
// зміна по кліку
let liList = document.querySelector(".carousel-items");
liList.addEventListener("click", function (e) {
  let target = e.target;
  let allLi = document.querySelectorAll(".carousel-item");
  let arr = [...allLi];
  arr.forEach((e) => {
    if (target.tagName === "IMG" && target.tagName !== "UL") {
      e.classList.remove("carousel-item-active");
      target.closest("li").classList.add("carousel-item-active");
    } else if (target.tagName === "LI" && target.tagName !== "UL") {
      e.classList.remove("carousel-item-active");
      target.classList.add("carousel-item-active");
    }
  });
  let activeData = document.querySelector(".carousel-item-active").children[0]
    .dataset.name;
  showContent(activeData);
  let photoSrc = document.querySelector(".carousel-item-active").children[0]
    .src;
  changePhoto(photoSrc);
});
//трошки анімувати переходи контенту
function showContent(activeData) {
  let divContent = $(".feedback-content");
  $(divContent).each((i, e) => {
    $(e).data("name") !== activeData
      ? $(e).slideUp(1000).removeClass("feedback-content-active")
      : $(e).slideDown(1000).addClass("feedback-content-active");
  });
}
//змінюємо активну фотографію
function changePhoto(src) {
  let activePhoto = document.querySelector(".feedback-creator-photo");
  activePhoto.src = src;
  return activePhoto;
}
//кнопка зліва
let reduceBtn = document.querySelector(".reduce-btn");
reduceBtn.addEventListener("click", function () {
  let allLi = document.querySelectorAll(".carousel-item");
  let arr = [...allLi];
  let activeLi = document.querySelector(".carousel-item-active");
  if (activeLi.previousElementSibling) {
    activeLi.classList.remove("carousel-item-active");
    activeLi.previousElementSibling.classList.add("carousel-item-active");
  } else {
    arr[arr.length - 1].classList.add("carousel-item-active");
    activeLi.classList.remove("carousel-item-active");
  }
  let activeData = document.querySelector(".carousel-item-active").children[0]
    .dataset.name;
  showContent(activeData);
  let photoSrc = document.querySelector(".carousel-item-active").children[0]
    .src;
  changePhoto(photoSrc);
});
//кнопка справа
let incraseBtn = document.querySelector(".increase-btn");
incraseBtn.addEventListener("click", function () {
  let allLi = document.querySelectorAll(".carousel-item");
  let arr = [...allLi];
  let activeLi = document.querySelector(".carousel-item-active");
  if (activeLi.nextElementSibling) {
    activeLi.classList.remove("carousel-item-active");
    activeLi.nextElementSibling.classList.add("carousel-item-active");
  } else {
    activeLi.classList.remove("carousel-item-active");
    arr[0].classList.add("carousel-item-active");
  }
  let activeData = document.querySelector(".carousel-item-active").children[0]
    .dataset.name;
  showContent(activeData);
  let photoSrc = document.querySelector(".carousel-item-active").children[0]
    .src;
  changePhoto(photoSrc);
});
